Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: MARC-Record
Upstream-Contact: Galen Charlton <gmcharlt@gmail.com>
Source: https://metacpan.org/release/MARC-Record

Files: *
Copyright: 2001-2007, Andy Lester <marc@petdance.com>
 2002-2007, Ed Summers <ehs@pobox.net>
 2003-2005, Eric Lease Morgan <emorgan@nd.edu>
 2003, Morbus Iff
 2004-2005, Bryan Baldus
 2004, Mark Jordan
 2007, Mike Rylander
 2007, Dan Scott
 2009, Bill Dueber
 2010-2013, Galen Charlton <gmcharlt@gmail.com>
 2010, Frédéric Demians <f.demians@tamil.fr>
 2010, Dan Wells
 2010, Alex Arnaud <alex.arnaud@biblibre.com>
 2010, Colin Campbell <colin.campbell@ptfs-europe.com>
 2013, Robin Sheat <robin@catalyst.net.nz>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2006-2017, gregor herrmann <gregoa@debian.org>
 2007, Vincent Danjean <vdanjean@debian.org>
 2008, Niko Tyni <ntyni@debian.org>
 2010, Chris Butler <chrisb@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
