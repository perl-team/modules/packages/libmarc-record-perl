libmarc-record-perl (2.0.7-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libmarc-record-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 21:20:43 +0100

libmarc-record-perl (2.0.7-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 16:21:44 +0100

libmarc-record-perl (2.0.7-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Chris Butler from Uploaders. Thanks for your work!
  * Remove Rene Mayorga from Uploaders. Thanks for your work!
  * New upstream release.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.0.0.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Fri, 04 Aug 2017 17:06:23 -0400

libmarc-record-perl (2.0.6-1) unstable; urgency=medium

  * New upstream release.
  * Drop spelling.patch, merged upstream.
  * debian/watch: use default regexp for matching upstream versions.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sun, 15 Dec 2013 18:50:47 +0100

libmarc-record-perl (2.0.5-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
    Fixes "FTBFS with perl 5.18: t/extra_controlfields.t"
    (Closes: #710801)
  * Update years of upstream and packaging copyright.
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Set Standards-Version to 3.9.4 (no changes).
  * Bump debhelper compatibility level to 8.
  * Add 'perl' to Build-Depends-Indep.
  * Update license stanzas in debian/copyright.
  * Add a patch to fix a spelling mistake.

 -- gregor herrmann <gregoa@debian.org>  Sun, 02 Jun 2013 20:01:23 +0200

libmarc-record-perl (2.0.3-1) unstable; urgency=low

  * New upstream release
  * Upped Standards-Version to 3.9.1 (no changes required)

 -- Chris Butler <chrisb@debian.org>  Sun, 27 Feb 2011 23:09:36 +0000

libmarc-record-perl (2.0.2-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Rene Mayorga ]
  * debian/control: update my email address.

  [ gregor herrmann ]
  * Change my email address.

  [ Chris Butler ]
  * New upstream release
  * Refreshed packaging with new dh7 small rules file
  * Switched to source format 3.0 (quilt)
  * Added myself to Uploaders
  * Upped Standards-Version to 3.8.4 (removed versioned build-dep on perl)

 -- Chris Butler <chrisb@debian.org>  Tue, 04 May 2010 23:01:00 +0100

libmarc-record-perl (2.0.0-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed:
    Homepage pseudo-field (Description); XS-Vcs-Svn fields.
  * debian/rules: delete /usr/lib/perl5 only if it exists.

  [ Rene Mayorga ]
  * Make package ready for perl 5.10 update
  * debian/control
    + Set standards-version to 3.7.3 ( no changes needed )
    + Raise debhelper version to 6
    + Add myself to uploaders
  * debian/rules
    + remove OPTIMIZE var, we are not using it
    + move tests suite from install to build target
    + use $@ when touching -stamp files
    + remove unused dh_ calls
    + don't install README, it only contains the same info as the pods
  * debian/copyright: use a dist-based URL
  * Improve debian/watch

  [ Niko Tyni ]
  * Remove unnecessary debian/dirs file.

 -- Niko Tyni <ntyni@debian.org>  Mon, 11 Feb 2008 12:30:18 +0200

libmarc-record-perl (2.0.0-1) unstable; urgency=low

  * New upstream release
    Lots of improvments and bug fixes
  * update watch file
  * add myself to Uploaders field
  * Bump Standard-Version to 3.7.2 (no changes needed)
  * Add libtest-pod-coverage-perl and libtest-pod-perl to Build-Depends so
    that all tests can be done at build time.

 -- Vincent Danjean <vdanjean@debian.org>  Fri, 26 Jan 2007 14:28:26 +0100

libmarc-record-perl (1.38-3) unstable; urgency=low

  * Initial upload to the Debian archive. (Closes: #251875)
  * Set debhelper compatibility level to 5.
  * Changed Maintainer to the Debian Perl Group before initial import to
    the pkg-perl svn repository.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 25 Feb 2006 19:36:57 +0100

libmarc-record-perl (1.38-2) unstable; urgency=low

  * Changed debian/control and debian/rules with dh-make-perl.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Mon,  2 Jan 2006 01:24:18 +0100

libmarc-record-perl (1.38-1) unstable; urgency=low

  * Initial release

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun,  1 Jan 2006 17:54:09 +0100
